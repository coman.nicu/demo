package tracker.exception.customExceptions;

import org.springframework.http.HttpStatus;

import java.util.List;

public class IllegalStateException extends TrackerException {
    public IllegalStateException(HttpStatus status, String message, List<String> errors) {
        super(status, message, errors);
    }

    public IllegalStateException(HttpStatus status, String message, String error) {
        super(status, message, error);
    }
}
