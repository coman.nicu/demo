import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './error-401-component.component.html',
  styleUrls: ['./error-401-component.component.css']
})
export class Error401Component implements OnInit {
  title = 'Whoops';


  constructor(private router: Router) {
  }

  ngOnInit() {
  }


}
