package tracker.services;

import tracker.dto.UserDto;
import tracker.model.User;

import java.util.List;

public interface UserService {

    UserDto save(final UserDto user);

    UserDto findById(final int id);

    UserDto update(final UserDto user, final int id);

    boolean delete(final int id);

    User findByUsername(String username);

    List<UserDto> getAll();
}

