import {Injectable} from '@angular/core';
import {Expense} from '../model/expense';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ExpenseService {

  expenseUrl = 'http://localhost:8080/api/expense';

  constructor(private http: HttpClient) {
  }

  addExpense(expense: Expense, budgetId: number) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('authToken')
    });
    const options = {headers: headers};
    return this.http.post<Expense>(this.expenseUrl + '/create?' + 'budgetId=' + budgetId, expense, options);
  }

  getExpenses(budgetId: string) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('authToken')
    });
    const options = {headers: headers};
    return this.http.get<Expense[]>(this.expenseUrl + '/' + budgetId + '/all', options);
  }

  removeExpense(budgetId: string, expenseId: number) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('authToken')
    });
    const options = {headers: headers};
    return this.http.post(this.expenseUrl + '/' + expenseId + '/delete?budgetId=' + budgetId, null, options);
  }

}
