package tracker.controller;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tracker.dto.BudgetDto;
import tracker.services.BudgetService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/budget")
public class BudgetController {

    @Autowired
    BudgetService budgetService;

    @GetMapping("/{id}/get")
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(
            httpMethod = "GET",
            value = "Find a budget by Id",
            response = BudgetDto.class
    )
    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "Ok")}
    )
    public BudgetDto getBudgetForUser(@NotNull @PathVariable
                             @ApiParam(value = "the id of the budget to be found", required = true) final int id) {
        return budgetService.getBudget(id);

    }

    @GetMapping("/{id}/all")
    @PreAuthorize("hasRole('USER')")
    public List<BudgetDto> getBudgetsForUser(@PathVariable final int id){
        return budgetService.getAllBudgetsForUser(id);
    }

    @PostMapping(value = "create/{income}/")
    @ApiOperation(
            httpMethod = "POST",
            value = "Add a budget to an existing user",
            response = BudgetDto.class,
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "created")})

    public BudgetDto createBudget(@NotNull @ApiParam("the id of the user") @RequestParam int userId,
                                  @NotNull @PathVariable @ApiParam("the income of the budget") String income) {
        return budgetService.makeBudget(BudgetDto
                        .builder()
                        .income(Double.valueOf(income))
                        .build(),
                userId);
    }

    @PostMapping(value = "{id}/delete")
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(
            httpMethod = "POST",
            value = "Delete a budget",
            response = Boolean.class
    )
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Deleted")})
    public boolean delete(@NotNull @RequestParam @ApiParam("the id of the budget to be deleted") int id,
                          @NotNull @RequestParam @ApiParam("the id of the user the budget belongs to") int userId) {
        return budgetService.remove(id, userId);
    }

    @PostMapping(value = "{id}/update")
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(
            httpMethod = "POST",
            value = "Update a budget",
            response = BudgetDto.class
    )
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Updated")})
    public BudgetDto update(@RequestBody @Valid BudgetDto budget, @RequestParam @ApiParam("the id of the budget") int id) {
        return budgetService.updateBudget(budget, id);

    }

    @GetMapping("/{id}/getReport")
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(
            httpMethod = "GET",
            value = "Get the report values of a budget",
            response = ResponseEntity.class
    )
    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "Ok")}
    )
    public Map<String, Double> getBudgetReport(@NotNull @PathVariable
                                               @ApiParam(value = "the id of the budget for which the report is desired", required = true) final int id) {
        return budgetService.getBudgetReport(id);

    }

}
