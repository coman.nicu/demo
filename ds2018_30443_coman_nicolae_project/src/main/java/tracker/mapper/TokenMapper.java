package tracker.mapper;

import org.springframework.stereotype.Component;
import tracker.dto.TokenDto;
import tracker.model.Token;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TokenMapper {


    public  Token toEntity(TokenDto dto) {
        return Token.builder()
                .email(dto.getEmail())
                .consumed(dto.isConsumed())
                .admin(dto.isAdmin())
                .token(dto.getToken())
                .build();
    }

    public  TokenDto toDto(Token entity) {
        return TokenDto.builder()
                .email(entity.getEmail())
                .consumed(entity.isConsumed())
                .token(entity.getToken())
                .admin(entity.isAdmin())
                .id(entity.getId())
                .build();
    }

    public List<Token> toEntityList(List<TokenDto> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    public List<TokenDto> toDtoList(List<Token> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }

}
