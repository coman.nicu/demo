package tracker.controller;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tracker.dto.ExpenseDto;
import tracker.services.ExpenseService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController()
@CrossOrigin
@RequestMapping("/expense")
public class ExpenseController {

    @Autowired
    private ExpenseService expenseService;

    @GetMapping("/{id}/get")
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(
            httpMethod = "GET",
            value = "Find an expense by id",
            response = ExpenseDto.class
    )
    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "Ok")}
    )
    public ExpenseDto getExpense(@NotNull @PathVariable
                                 @ApiParam(value = "the id of the expense to be found", required = true) final int id) {
        return expenseService.findExpense(id);
    }

    @PostMapping(value = "/create")
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(
            httpMethod = "POST",
            value = "Add an expense to an existing budget",
            response = ExpenseDto.class,
            consumes = "application/json"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "created")})

    public ExpenseDto createBudget(@NotNull @ApiParam("the id of the budget") @RequestParam int budgetId,
                                   @NotNull @RequestBody @ApiParam("the income of the budget") ExpenseDto expenseDto) {
        return expenseService.addExpense(expenseDto, budgetId);
    }

    @PostMapping(value = "{id}/delete")
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(
            httpMethod = "POST",
            value = "Delete an expense",
            response = Boolean.class
    )
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Deleted")})
    public void delete(@NotNull @PathVariable @ApiParam("the id of the expense to delete") int id,
                       @NotNull @RequestParam @ApiParam("the id of the budget the expense belongs to") int bId) {
        expenseService.deleteExpense(id, bId);
    }

    @PostMapping(value = "{id}/update")
    @PreAuthorize("hasRole('USER')")
    @ApiOperation(
            httpMethod = "POST",
            value = "Update am Expense",
            response = ExpenseDto.class
    )
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Updated")})
    public ExpenseDto update(@RequestBody @Valid @ApiParam("the updated expense except its id") ExpenseDto expense,
                             @RequestParam @ApiParam("the id of the expense") int expenseId) {
        return expenseService.updateExpense(expense, expenseId);

    }

}


