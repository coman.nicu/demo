package tracker.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import tracker.exception.customExceptions.IllegalStateException;
import tracker.exception.customExceptions.ResourceNotFoundException;
import tracker.exception.customExceptions.TrackerException;
import tracker.exception.customExceptions.WrongDataException;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class WebRestControllerAdvice extends ResponseEntityExceptionHandler {


    @ExceptionHandler(value = ResourceNotFoundException.class)
    public ResponseEntity<Object> handleIllegalArgumentException(final TrackerException ex) {
        return buildResponseEntity(TrackerEerrorResponse.builder()
                .message(ex.getMessage())
                .error_code("404")
                .status(ex.getStatus())
                .detail(buildErrorMessage(ex))
                .build()
        );
    }

    @ExceptionHandler(value = WrongDataException.class)
    public ResponseEntity<Object> handleInvalidDataException(final TrackerException ex) {
        return buildResponseEntity(TrackerEerrorResponse.builder()
                .message(ex.getMessage())
                .error_code("406")
                .status(ex.getStatus())
                .detail(buildErrorMessage(ex))
                .build()
        );
    }

    @ExceptionHandler({AuthenticationException.class})
    public ResponseEntity<String> handleAuthenticationException(AuthenticationException e) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
    }

    @ExceptionHandler(value = IllegalStateException.class)
    public ResponseEntity<Object> handleIllegalStateException(final TrackerException ex) {
        return buildResponseEntity(TrackerEerrorResponse.builder()
                .message(ex.getMessage())
                .error_code("403")
                .status(ex.getStatus())
                .detail(buildErrorMessage(ex))
                .build()
        );
    }

    @ExceptionHandler(value = NumberFormatException.class)
    public ResponseEntity<Object> handleNUmberFOrmatException(final NumberFormatException ex) {
        return buildResponseEntity(TrackerEerrorResponse.builder()
                .message("Please provide correct input in the swagger ui")
                .error_code("403")
                .build()
        );

    }

    @ExceptionHandler(value = UsernameNotFoundException.class)
    public ResponseEntity<Object> handleUsernameNotFoundException(final UsernameNotFoundException ex) {
        return buildResponseEntity(TrackerEerrorResponse.builder()
                .message(ex.getMessage())
                .error_code("404")
                .build()
        );
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {

        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors().stream()
                .map(f -> new FieldError(f.getObjectName(), f.getField(), f.getCode()))
                .collect(Collectors.toList());

        return handleExceptionInternal(ex, "Input data wrong format in fields: " + fieldErrors + "\n",
                new HttpHeaders(), HttpStatus.UNPROCESSABLE_ENTITY, request);
    }


    private ResponseEntity<Object> buildResponseEntity(final TrackerEerrorResponse error) {
        return new ResponseEntity<>(error, error.getStatus());
    }

    private String buildErrorMessage(final TrackerException ex) {
        String s = "";
        for (String err : ex.getErrors()) {
            s += " " + err;
        }
        return s;
    }
}

