package tracker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tracker.model.User;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    public User findByUsername(String username);

    public User findByEmail(String email);

    public List<User> findAllByUsernameAndIdIsNot(String username, int id);

    public List<User> findAllByEmailAndIdIsNot(String username, int id);
}
