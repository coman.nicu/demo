package tracker.exception;

import lombok.Builder;
import lombok.Data;

import org.springframework.http.HttpStatus;

@Data
public class TrackerEerrorResponse {
    private HttpStatus status;
    private String error_code;
    private String message;
    private String detail;

    @Builder
    public TrackerEerrorResponse(HttpStatus status, String error_code, String message, String detail) {
        this.status = status;
        this.error_code = error_code;
        this.message = message;
        this.detail = detail;
    }
}
