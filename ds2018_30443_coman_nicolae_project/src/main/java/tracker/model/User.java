package tracker.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "users")
public class User {

    @Enumerated(EnumType.STRING)
    UserRole userRole;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String username;
    private String password;
    private String email;
    private double currentIncome;
    @OneToMany(mappedBy = "user", orphanRemoval = true)
    private List<Budget> budgets;

}
