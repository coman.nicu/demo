package tracker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tracker.model.Token;

@Repository
public interface TokenRepository extends JpaRepository<Token, Integer> {

    public Token findByEmail(String email);

    public Token findByToken(String token);

    public void deleteByEmail(String email);
}
