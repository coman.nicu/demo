package tracker.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tracker.dto.BudgetDto;
import tracker.exception.customExceptions.IllegalStateException;
import tracker.exception.customExceptions.ResourceNotFoundException;
import tracker.mapper.BudgetMapper;
import tracker.model.Budget;
import tracker.model.Expense;
import tracker.model.ExpenseType;
import tracker.model.User;
import tracker.repository.BudgetRepository;
import tracker.repository.ExpenseRepository;
import tracker.repository.UserRepository;
import tracker.services.BudgetService;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class BudgetServiceImpl implements BudgetService {

    @Autowired
    private BudgetRepository budgetRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ExpenseRepository expenseRepository;

    @Autowired
    private BudgetMapper budgetMapper;


    @Override
    public BudgetDto makeBudget(BudgetDto budget, int userId) {
        if (!checkIfUserExists(userId)) {
            throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "The user with the id: " + userId + " does not exist", "Not Found");
        }

        if (!isAffordable(budget.getIncome(), userId)) {
            throw new IllegalStateException(HttpStatus.FORBIDDEN, "The user does not afford this budget. " +
                    "His current income is " + getCurrentIncome(userId), "Forbidden");
        }
        updateUserBudget(budget.getIncome(), userId);
        Budget entity = budgetMapper.toEntity(budget);
        entity.setUser(userRepository.findById(userId).get());
        return budgetMapper.toDto(budgetRepository.save(entity));
    }

    private boolean isAffordable(double income, int userId) {
        User user = userRepository.findById(userId).get();
        return user.getCurrentIncome() >= income;
    }

    @Override
    public boolean remove(final int budgetId, final int userId) {
        if (!doesBudgetExist(budgetId)) {
            throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "The budget with the id: " + budgetId + " does not exist", "Not Found");
        }
        checkIfUserExists(userId);
        Budget budget = budgetRepository.findById(budgetId).get();
        updateUserBudget(-budget.getIncome(),userId);
        budgetRepository.deleteById(budgetId);
        return true;
    }

    @Override
    public BudgetDto getBudget(int budgetId) {
        if (!doesBudgetExist(budgetId)) {
            throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "The budget with the id: " + budgetId + " does not exist", "Not Found");
        }
        return budgetMapper.toDto(budgetRepository.findById(budgetId).get());
    }
    /*
    * does no
    * */
    @Override
    public BudgetDto updateBudget(BudgetDto budget, int budgetId) {
        if (!doesBudgetExist(budgetId)) {
            throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "The budget with the id: " + budgetId + " does not exist", "Not Found");
        }
        if (budget.getIncome() < 0) {
            throw new IllegalStateException(HttpStatus.FORBIDDEN, "The budget cannot be negative. ", "Forbidden");
        }

        /*get entity containing all expenses, copy them in the dto, convert back dto to entity, update
         I do this in because makeExpense/removeExpense methods should be used to alter expenses in a budget, not update */
        Budget entity = budgetRepository.findById(budgetId).get();
        double oldEntityBudget = entity.getIncome();
        copyExpensesIds(entity, budget);
        updateFields(budget, entity);
        updateUserBudgetWhenBudgetIsUpdated(budget.getIncome(), oldEntityBudget, budget.getUserId());
        return budgetMapper.toDto(budgetRepository.save(entity));
    }

    @Override
    public Map<String, Double> getBudgetReport(int budgetId) {
        Map<String, Double> report = new HashMap<>();
        List<Expense> expenseList = expenseRepository.findAllByBudgetId(budgetId);
        if (expenseList.isEmpty()) {
            throw new IllegalStateException(HttpStatus.FORBIDDEN, "There are no expenses for this budget", "Forbidden");
        }
        double totalValue = 0.0;
        for (ExpenseType type : ExpenseType.values()) {
            double value = expenseList.stream()
                    .filter(expense -> expense.getExpenseType() == type)
                    .map(Expense::getValue)
                    .mapToDouble(Double::valueOf)
                    .sum();
            report.put(type.toString(), value);
            totalValue += value;
        }

        //normalize values in order to obtain percents
        normalizeMapValues(report, totalValue);
        return report;
    }

    @Override
    public List<BudgetDto> getAllBudgetsForUser(int id) {
        User user = userRepository.findById(id).get();
        List<Budget> userBudgets = budgetRepository
                .findAllByIdIn(user.getBudgets().stream().map(Budget::getId).collect(Collectors.toList()));
        return budgetMapper.toDtoList(userBudgets);
    }

    private void normalizeMapValues(Map<String, Double> report, double totalValue) {
        Iterator keySetIterator = report.keySet().iterator();
        while (keySetIterator.hasNext()) {
            String key = (String) keySetIterator.next();
            double oldValue = report.get(key);
            report.put(key, oldValue / totalValue);
        }
    }

    private boolean checkIfUserExists(final int id) {
        if (!userRepository.findById(id).isPresent()) {
            return false;
        }
        return true;
    }


    private boolean doesBudgetExist(int budgetId) {
        if (!budgetRepository.findById(budgetId).isPresent()) {
            return false;
        }
        return true;
    }

    private double getCurrentIncome(int userId) {
        return userRepository.findById(userId).get().getCurrentIncome();
    }

    private void updateUserBudget(double income, int userId) {
        User user = userRepository.findById(userId).get();
        user.setCurrentIncome(user.getCurrentIncome() - income);
        userRepository.save(user);
    }

    private void copyExpensesIds(Budget from, BudgetDto to) {
        to.setExpensesIds(new ArrayList<>());
        from.getExpenses().stream().map(l -> to.getExpensesIds().add(l.getId()));
    }

    /*
    * does not copy user and expenses
    * */
    private void updateFields(BudgetDto from, Budget to) {
        to.setIncome(from.getIncome());
        to.setName(from.getBudgetName());
    }

    private void updateUserBudgetWhenBudgetIsUpdated(double newIncome, double oldIncome, int userId) {
        User user = userRepository.findById(userId).get();
        double userIncome = user.getCurrentIncome();
        if ((userIncome + oldIncome) - newIncome < 0) {
            throw new IllegalStateException(HttpStatus.FORBIDDEN, "User cannot afford this income. ", "Forbidden");
        }
        user.setCurrentIncome((userIncome + oldIncome) - newIncome);
        userRepository.save(user);
    }

}
