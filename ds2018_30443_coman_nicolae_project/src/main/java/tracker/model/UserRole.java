package tracker.model;

public enum UserRole {
    ADMIN,
    USER
}
