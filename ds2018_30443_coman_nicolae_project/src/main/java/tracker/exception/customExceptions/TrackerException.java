package tracker.exception.customExceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper=false)
public class TrackerException extends RuntimeException  {

    private HttpStatus status;
    private String message;
    private List<String> errors;

    public TrackerException(HttpStatus status, String message, List<String> errors) {
        super();
        this.status = status;
        this.message = message;
        this.errors = errors;
    }

    public TrackerException(HttpStatus status, String message, String error) {
        super();
        this.status = status;
        this.message = message;
        errors = Arrays.asList(error);
    }
}
