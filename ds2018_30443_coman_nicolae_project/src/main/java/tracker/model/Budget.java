package tracker.model;


import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.singletonList;
import static java.util.Objects.isNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "budgets")
public class Budget {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private double income;

    private String name;

    @ManyToOne
    @JoinColumn(name = "fk_budget")
    private User user;

    @OneToMany(mappedBy = "budget", orphanRemoval = true)
    private List<Expense> expenses;

    public void setUser(final User user) {
        this.user = user;
        if(isNull(user.getBudgets())){
            user.setBudgets(singletonList(this));
        } else {
            List<Budget> userBudgets = new ArrayList<>(user.getBudgets());
            userBudgets.add(this);
            user.setBudgets(userBudgets);
        }
    }
}
