package tracker.utils.config.cache;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "redis.prop")
@Configuration
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
public class RedisProperties {

    String hostname;

    int port;

}
