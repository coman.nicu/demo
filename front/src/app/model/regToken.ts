export class RegToken {
  id: number;
  token: string;
  email: string;
  consumed: boolean;
  admin: boolean;
}
