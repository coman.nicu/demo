package tracker.services;

import tracker.dto.TokenDto;
import tracker.dto.UserDto;

public interface RegistrationService {

    public TokenDto sendToken(String destEmail, boolean isAdmin);

    public UserDto register(UserDto userDto, String token);

}
