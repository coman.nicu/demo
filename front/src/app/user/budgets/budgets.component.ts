import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {NgForm} from '@angular/forms';
import {BudgetService} from '../../services/budget.service';
import {HttpErrorResponse} from '@angular/common/http';
import {Budget} from '../../model/budget';
import {User} from '../../model/user';
import {UserService} from '../../services/user.service';
import {TrackerException} from '../../model/exception';
import swal from 'sweetalert';
import {Expense} from '../../model/expense';
import {ExpenseService} from '../../services/expense.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './budgets.component.html',
  styleUrls: ['./budgets.component.css']
})
export class BudgetsComponent implements OnInit {

  budgets: Budget[] = [];
  currentUser: User;
  errorBody: HttpErrorResponse;
  displayedColumns: string[] = ['budgetId', 'name', 'income', 'remove', 'report', 'add', 'edit'];
  receivedException: TrackerException;
  clickedBudget: Budget;
  expenseTypes = [{name: 'FOOD', value: 'FOOD'}, {name: 'BILLS', value: 'BILLS'},
    {name: 'FINES', value: 'FINES'}, {name: 'ENTERTAINMENT', value: 'ENTERTAINMENT'},
    {name: 'DRINKS', value: 'DRINKS'}, {name: 'CLOTHES', value: 'CLOTHES'},
    {name: 'TECH', value: 'TECH'}];
  expenseType = {name: '', value: null};

  public pieChartLabels = [];
  public pieChartData: number[] = [];
  public pieChartType = 'doughnut';
  public pieChartOptions: any = {
    'backgroundColor': [
      '#FF6384',
      '#4BC0C0',
      '#FFCE56',
    ],
    'legend': {
      position: 'bottom',
    }
  };

  constructor(private route: ActivatedRoute, private budgetService: BudgetService, private dialog: MatDialog
    , private userService: UserService, private expenseService: ExpenseService, private router: Router) {
  }

  ngOnInit() {
    if (localStorage.getItem('role') !== 'ROLE_USER') {
      this.router.navigate(['401']);
    } else {
      const idString = localStorage.getItem('userId');
      const idNumber = Number(idString);
      this.budgetService.getBudgets(idString).subscribe(data => this.budgets = data, error => {
        this.errorBody = error;
        this.receivedException = this.errorBody.error;
        swal('Bummer', this.receivedException.message, 'error');
      });
      this.userService.getUserById(idNumber).subscribe(data => this.currentUser = data, error => {
        this.errorBody = error;
        this.receivedException = this.errorBody.error;
        swal('Bummer', this.receivedException.message, 'error');
      });
    }
  }

  showDialog(templateRef) {
    const dialogRef = this.dialog.open(templateRef, {
      height: '360px',
      width: '270px',
    });
  }

  showDialogRememberRow(templateRef, budget) {
    this.clickedBudget = budget;
    const dialogRef = this.dialog.open(templateRef, {
      height: '360px',
      width: '270px',
    });
  }

  onSubmitCreate(form: NgForm) {
    let addedBudget = {
      budgedId: 0,
      income: 0,
      budgetName: '',
      userId: 0,
      expensesIds: []
    };

    let auxBudget = {budgetName: '', income: ''};
    auxBudget = form.value;


    this.budgetService.addBudget(form.value, this.currentUser.userId).subscribe(data => {
        addedBudget = data;
        addedBudget.budgetName = auxBudget.budgetName;
        console.log();
        this.budgetService.updateBudget(addedBudget).subscribe(data2 => {
          this.updateData();
        });

      },
      error => {
        this.errorBody = error;
        this.receivedException = this.errorBody.error;
        swal('Bummer', this.receivedException.message, 'error');
      }
    );

  }

  removeBudget(budget: Budget) {
    this.budgetService.removeBudget(budget.budgedId, budget.userId).subscribe(() => this.updateData());
  }

  updateData() {
    const idString = localStorage.getItem('userId');
    this.budgetService.getBudgets(idString).subscribe(data => this.budgets = data, error => {
      this.errorBody = error;
      this.receivedException = this.errorBody.error;
      swal('Bummer', this.receivedException.message, 'error');
    });
    this.userService.getUserById(Number(idString)).subscribe(data => this.currentUser = data, error => {
      this.errorBody = error;
      this.receivedException = this.errorBody.error;
      swal('Bummer', this.receivedException.message, 'error');
    });
  }

  showReport(budget: Budget, templateRef) {
    this.budgetService.getReport(budget.budgedId).subscribe(data => {
      this.pieChartLabels = Array.from(Object.keys(data));
      this.pieChartData = Array.from(Object.values(data));
      const dialogRef = this.dialog.open(templateRef, {
        height: '610px',
        width: '500px',
      });
    }, error => {
      this.errorBody = error;
      this.receivedException = this.errorBody.error;
      swal('Bummer', this.receivedException.message, 'error');
    });

  }

  addExpense(form: NgForm) {
    const exp: Expense = form.value;
    console.log(exp);
    this.expenseService.addExpense(form.value, this.clickedBudget.budgedId).subscribe(() => this.updateData(), error => {
      this.errorBody = error;
      this.receivedException = this.errorBody.error;
      swal('Bummer', this.receivedException.message, 'error');
    });
  }

  showExpenses(budget: Budget) {
    this.router.navigate(['expensesOf', budget.budgedId]);
  }

  onSubmitUpdate(form: NgForm) {
    const formBudget: Budget = form.value;
    formBudget.budgedId = this.clickedBudget.budgedId;
    formBudget.userId = this.currentUser.userId;
    console.log(formBudget);
    this.budgetService.updateBudget(formBudget).subscribe(() => this.updateData(), error => {
      this.errorBody = error;
      this.receivedException = this.errorBody.error;
      swal('Bummer', this.receivedException.message, 'error');
    });
  }

}
