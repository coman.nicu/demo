package tracker.services;


import lombok.experimental.FieldDefaults;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;
import tracker.dto.UserDto;
import tracker.model.User;
import tracker.repository.UserRepository;
import tracker.services.implementations.UserServiceImpl;

import static lombok.AccessLevel.PRIVATE;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static tracker.model.UserRole.USER;

@FieldDefaults(level = PRIVATE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class UserServiceImplIT {

    final static UserDto USER_DTO = UserDto.builder()
            .email("testEamil@email.com")
            .currentIncome(100000)
            .password("parola123")
            .username("nickname123")
            .userRole(USER)
            .build();

    final static UserDto USER_2_DTO = UserDto.builder()
            .email("testEamil2@email.com")
            .currentIncome(200000)
            .password("parola1234")
            .username("nickname1234")
            .userRole(USER)
            .build();

    @Autowired
    UserRepository userRepository;
    @Autowired
    UserServiceImpl userService;

    @Autowired
    RedisTemplate<String, UserDto> redisTemplate;

    @Before
    public void setUp() {
        userRepository.deleteAll();
    }

    @Test
    public void givenValidUserDto_whenSaveUser_userIsSaved() {
        // Given

        // When
        final UserDto savedUser = userService.save(USER_DTO);

        // Then
        assertNotNull(savedUser);
    }

    @Test
    public void givenUserExists_whenFindUserById_userDtoIsReturned() {
        // Given
        final UserDto savedUser = userService.save(USER_DTO);

        // When
        final User foundUser = userRepository.findById(savedUser.getUserId()).get();

        // Then
        assertNotNull(foundUser);
        assertEquals(savedUser.getUserId(), foundUser.getId());
        assertEquals(savedUser.getUsername(), foundUser.getUsername());
        assertEquals(savedUser.getPassword(), foundUser.getPassword());
    }

    @Test
    public void givenUserExists_whenFindUserById_userIsCached() {
        // Given
        final UserDto savedUser = userService.save(USER_2_DTO);

        // When
        userService.findById(savedUser.getUserId());

        // Then
        ValueOperations valueOperations = redisTemplate.opsForValue();

        UserDto cachedUser = (UserDto)valueOperations.get("users::"+savedUser.getUserId());
        assertNotNull(cachedUser);
        assertEquals(savedUser.getUsername(), cachedUser.getUsername());
    }
}
