package tracker.services;

import tracker.dto.BudgetDto;

import java.util.List;
import java.util.Map;

public interface BudgetService {
    BudgetDto makeBudget(final BudgetDto budget, final int userID);

    boolean remove(final int budgetId, final int userId);

    BudgetDto getBudget(final int budgetId);

    BudgetDto updateBudget(final BudgetDto budget, final int budgetId);

    Map<String, Double> getBudgetReport(int budgetId);

    List<BudgetDto> getAllBudgetsForUser(int id);
}
