package tracker.utils.validators.impl;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import tracker.utils.validators.IsAplhaNumeric;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static org.springframework.util.StringUtils.isEmpty;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class IsAplhaNumericValidator implements ConstraintValidator<IsAplhaNumeric, String> {


    @Override
    public void initialize(IsAplhaNumeric constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        value = value.replaceAll("[^0-9]", "");
        value = value.replaceAll("[^a-zA-Z]", "");
        return isEmpty(value);
    }
}
