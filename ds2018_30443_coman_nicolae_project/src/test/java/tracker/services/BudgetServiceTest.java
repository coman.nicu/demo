package tracker.services;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import tracker.dto.BudgetDto;
import tracker.mapper.BudgetMapper;
import tracker.model.Budget;
import tracker.model.User;
import tracker.model.UserRole;
import tracker.repository.BudgetRepository;
import tracker.repository.UserRepository;
import tracker.services.implementations.BudgetServiceImpl;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class BudgetServiceTest {

    final static int USER_ID = 1;
    final static User USER = User.builder()
            .id(USER_ID)
            .username("neeecu")
            .email("neeecu@email.com")
            .password("alabalaportocala")
            .userRole(UserRole.USER)
            .currentIncome(100000)
            .build();

    final static Budget BUDGET = Budget.builder()
            .id(USER_ID)
            .income(45000)
            .name("Pomninoc")
            .build();

    @Mock
    UserRepository userRepository;
    @Mock
    BudgetRepository budgetRepository;
    @Mock
    BudgetMapper budgetMapper;
    @InjectMocks
    BudgetServiceImpl budgetService;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void givenExistentUser_whenMakeBudget_userIncomeUpdated() {
        // Given
        final BudgetDto budgetToCreate = BudgetDto.builder()
                .budgetName("Pomninoc")
                .income(45000)
                .userId(USER_ID)
                .build();

        when(userRepository.findById(any())).thenReturn(Optional.of(USER));
        when(userRepository.save(any())).then(args -> args.getArgument(0));
        when(budgetRepository.save(any())).then(args -> args.getArgument(0));
        when(budgetMapper.toEntity(budgetToCreate)).
                thenReturn(BUDGET);
        when(budgetMapper.toDto(any()))
                .thenReturn(budgetToCreate);

        // When
        final BudgetDto result = budgetService.makeBudget(budgetToCreate, USER_ID);

        // Then
        assertNotNull(result);
        assertEquals(budgetToCreate.getBudgetName(), result.getBudgetName());
        assertEquals(budgetToCreate.getUserId(), result.getUserId());
        assertEquals(budgetToCreate.getIncome(), result.getIncome(), 0.1);
    }

    @Test
    public void givenExistingBudget_whenGetBudget_budgetDtoIsReturned() {
        // Given
        final int budgetId = 1;
        final BudgetDto budgetToReturn = BudgetDto.builder()
                .budgetName("Pomninoc")
                .income(45000)
                .userId(USER_ID)
                .build();

        when(budgetRepository.findById(any())).thenReturn(Optional.of(BUDGET));
        when(budgetMapper.toDto(any())).thenReturn(budgetToReturn);

        // When
        final BudgetDto resultDto = budgetService.getBudget(budgetId);

        // Then
        // Then
        assertNotNull(resultDto);
        assertEquals(budgetToReturn.getBudgetName(), budgetToReturn.getBudgetName());
        assertEquals(budgetToReturn.getUserId(), budgetToReturn.getUserId());
        assertEquals(budgetToReturn.getIncome(), budgetToReturn.getIncome(), 0.1);
    }

}
