package tracker.repository;

import lombok.experimental.FieldDefaults;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tracker.model.User;
import tracker.model.UserRole;

import static lombok.AccessLevel.PRIVATE;
import static org.junit.jupiter.api.Assertions.assertNotNull;


@FieldDefaults(level = PRIVATE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class UserRepositoryIT {

    final static User USER = User.builder()
            .username("neeecu")
            .email("neeecu@email.com")
            .password("alabalaportocala")
            .userRole(UserRole.USER)
            .currentIncome(100000)
            .build();

    @Autowired
    UserRepository userRepository;

    @Test
    public void givenValidUser_whenSaveUser_userIsSaved() {
        // Given

        // When
        final User savedUser = userRepository.save(USER);

        // Then
        assertNotNull(savedUser);
    }
}
