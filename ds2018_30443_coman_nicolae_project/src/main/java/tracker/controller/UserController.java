package tracker.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tracker.dto.UserDto;
import tracker.exception.customExceptions.ResourceNotFoundException;
import tracker.services.BudgetService;
import tracker.services.UserService;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

import static java.util.Objects.isNull;

@RestController
@CrossOrigin
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private RedisTemplate<String, Long> redisTemplate;

    @GetMapping("/all")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(httpMethod = "GET", value = "Get all users")
    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "Ok")}
    )
    public ResponseEntity<List<UserDto>> getAllUsers() {
        return new ResponseEntity<>(userService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}/get")
    @ApiOperation(
            httpMethod = "GET",
            value = "Find an user by id",
            response = UserDto.class
    )
    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "Ok")}
    )
    public UserDto getUser(@NotNull @PathVariable
                           @ApiParam(value = "the id of the user to be found", required = true) final int id) throws InterruptedException {
        final long accessingThread = Thread.currentThread().getId();
        log.info("Thread {} accessed the get user endpoint", accessingThread);

        Long currentWorkingThread = redisTemplate.opsForValue().get("currentThread");
        UserDto foundUser = null;

        if (currentWorkingThread == null || currentWorkingThread == -1L) {
            log.info("No other thread is executing the endpoint, accessing thread {} locks the endpoint!", accessingThread);
            redisTemplate.opsForValue().set("currentThread", accessingThread);
            foundUser = userService.findById(id);
            Thread.sleep(5000);
            log.info("The thread {} releases the endpoint", accessingThread);
            redisTemplate.opsForValue().set("currentThread", -1L);
            return foundUser;
        }

        log.debug("Currently, thread {} is executing the endpoint", currentWorkingThread);
        while (currentWorkingThread != -1L) {
            Thread.sleep(100);
            log.info("Blocking thread {} querying cache to see if it can proceed. ", accessingThread);
            currentWorkingThread = redisTemplate.opsForValue().get("currentThread");
            log.info("The current working thread is {}", currentWorkingThread);
        }
        log.info("No other thread is executing the endpoint anymore, " +
                "accessing thread {} locks the endpoint!", accessingThread);
        redisTemplate.opsForValue().set("currentThread", accessingThread);
        foundUser = userService.findById(id);
        Thread.sleep(5000);
        log.info("The thread {} releases the endpoint", accessingThread);
        redisTemplate.opsForValue().set("currentThread", -1L);
        return foundUser;

    }

    @PostMapping(value = "/save", consumes = "application/json")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(
            httpMethod = "POST",
            value = "UserDto",
            response = UserDto.class,
            consumes = "application/json"
    )
    @ApiResponses(
            value = {
                    @ApiResponse(code = 201, message = "Created"),
                    @ApiResponse(code = 406, message = "User with given username/password already exists"),
                    @ApiResponse(code = 422, message = "Unprocessable entity")
            })
    public UserDto saveUser(@RequestBody @Valid @ApiParam("dto of the user to be saved") final UserDto user) {
        return userService.save(user);

    }


    @PostMapping("/{id}/update")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(
            httpMethod = "POST",
            value = "Update an User",
            response = UserDto.class,
            consumes = "application/json"
    )
    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "Updated")}
    )

    public UserDto update(@NotNull @PathVariable @ApiParam("the id of the user to be updated") final int id,
                          @RequestBody @Valid @ApiParam("Dto with updated fields") final UserDto user) {
        return userService.update(user, id);

    }

    @GetMapping("/{id}/delete")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(
            httpMethod = "DELETE",
            value = "Delete an user"
    )
    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "Deleted"),
                    @ApiResponse(code = 404, message = "NotFound")
            })
    public ResponseEntity<String> delete(@NotNull @PathVariable @ApiParam("the id of the user to be deleted") int id) {
        if (userService.delete(id)) {
            return new ResponseEntity<>("Deleted", HttpStatus.OK);
        }
        throw new ResourceNotFoundException(HttpStatus.NOT_FOUND, "User with id = " + id + " not found", "Not Found");
    }

}

