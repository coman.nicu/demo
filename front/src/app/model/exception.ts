export class TrackerException {
  status: string;
  message: string;
  errors: string[];
}
