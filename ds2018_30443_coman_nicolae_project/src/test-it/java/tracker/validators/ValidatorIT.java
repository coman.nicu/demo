package tracker.validators;

import lombok.experimental.FieldDefaults;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tracker.dto.UserDto;
import tracker.model.UserRole;
import tracker.utils.validators.groups.AdminInfo;
import tracker.utils.validators.groups.UserInfo;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

import static lombok.AccessLevel.PRIVATE;
import static org.junit.jupiter.api.Assertions.assertEquals;

@FieldDefaults(level = PRIVATE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class ValidatorIT {

    private final static UserDto USER = UserDto.builder()
            .email("testEmail@email.com")
            .currentIncome(100000)
            .password("paro@la123")
            .username("nickname123")
            .userRole(UserRole.USER)
            .build();

    private final static UserDto VALID_ADMIN = UserDto.builder()
            .email("email@email123.com")
            .username("a")
            .password("p")
            .currentIncome(12323)
            .userRole(UserRole.ADMIN)
            .build();

    @Autowired
    Validator validator;

    @Test
    public void givenUserWithInvalidPassword_whenValidateUser_shouldReturnViolationForPassword() {
        // Given

        // When
        validator.validate(USER).forEach(
                // Then
                violation -> assertEquals("password", violation.getPropertyPath().toString())
        );
    }

    @Test
    public void givenValidAdminData_whenValidateAdmin_noViolationsAreFound() {
        // Given

        // When
        Set<ConstraintViolation<UserDto>> violations = validator.validate(VALID_ADMIN, AdminInfo.class);

        // Then
        assertEquals(0, violations.size());
    }

    @Test
    public void givenValidAdminData_whenValidateUser_violationsAreFound() {
        // Given

        // When
        Set<ConstraintViolation<UserDto>> violations = validator.validate(VALID_ADMIN, UserInfo.class);

        // Then
        assertEquals(2, violations.size());
    }

}
