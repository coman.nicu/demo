package tracker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tracker.model.Expense;

import java.util.List;

@Repository
public interface ExpenseRepository extends JpaRepository<Expense,Integer> {

    public List<Expense> findAllByIdIn(final List<Integer> Ids);

    public List<Expense> findAllByBudgetId(final int budgetId);

    public Expense findById(final int expenseId);
}
