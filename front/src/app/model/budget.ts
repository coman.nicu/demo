export class Budget {
  budgedId: number;
  income: number;
  budgetName: string;
  userId: number;
  expensesIds: number[];
}
