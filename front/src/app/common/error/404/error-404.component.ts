import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './error-404-component.html',
  styleUrls: ['./error-404-component.css']
})
export class Error404Component implements OnInit {
  title = 'Whoops';


  constructor(private router: Router) {
  }

  ngOnInit() {
  }


}
