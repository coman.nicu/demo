export class JwtToken {
  token: string;
  role: string;
  userId: string;
}
