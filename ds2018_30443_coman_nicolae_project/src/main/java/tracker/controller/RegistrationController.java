package tracker.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import tracker.dto.TokenDto;
import tracker.dto.UserDto;
import tracker.repository.UserRepository;
import tracker.services.RegistrationService;

import javax.validation.constraints.NotNull;

@RestController()
@CrossOrigin
@RequestMapping()
public class RegistrationController {

    @Autowired
    private RegistrationService registrationService;

    @Autowired
    private UserRepository userRepository;

    @PostMapping(value = "/sendToken")
    @PreAuthorize("hasRole('ADMIN')")
    @ApiOperation(
            httpMethod = "POST",
            value = "Send a token by email",
            response = TokenDto.class,
            consumes = "application/json"
    )
    @ApiResponses(
            value = {@ApiResponse(code = 200, message = "Sent")}
    )
    public TokenDto sendEmail(@ApiParam @RequestParam("destEmail") final String destEmail,
                              @RequestParam("isAdmin") @ApiParam("is this user going to be an admin?") boolean isAdmin) {
        return registrationService.sendToken(destEmail, isAdmin);
    }

    @PostMapping(value = "/register")
    @ApiOperation(
            httpMethod = "POST",
            value = "User registration based on token",
            response = UserDto.class,
            consumes = "application/json"
    )
    @ApiResponses(
            value = {@ApiResponse(code = 201, message = "Created")}
    )
    public UserDto registerUser(@RequestBody @ApiParam("Dto of the user to be saved,except id and budgets") UserDto dto,
                                @RequestParam("token") @NotNull String token) {
        return registrationService.register(dto, token);
    }

}
