import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {JwtToken} from '../../model/jwtToken';
import {UtilsService} from '../../services/utils.service';
import {HttpErrorResponse} from '@angular/common/http';
import swal from 'sweetalert';
import {TrackerException} from '../../model/exception';


@Component({
  selector: 'app-users',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  errorBody: HttpErrorResponse;
  receivedException: TrackerException;

  constructor(private router: Router, private utilsService: UtilsService) {
  }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    this.utilsService.register(form).subscribe(() => {
      swal('Registration Complete', '', 'success');
      this.router.navigate(['login']);
    }, error => {
      this.errorBody = error;
      this.receivedException = this.errorBody.error;
      const errorString: string = JSON.stringify(this.errorBody.error);
      console.log(errorString);
      let messageString = '';
      if (errorString.indexOf('password') !== -1) {
        messageString = messageString + 'The password field must contain at least 6 characters\n ';
      }
      if (errorString.indexOf('email') !== -1) {
        messageString = messageString + 'Please provide a valid email address\n ';
      }
      if (errorString.indexOf('username') !== -1) {
        messageString = messageString + 'The username field must contain at least 6 characters\n ';
      }
      if (messageString.length === 0) {
        swal('Bummer', 'Something went wrong. Please fill the form again carefully', 'error');

      } else {
        swal('Bummer', messageString, 'error');
      }
    });
  }
}
